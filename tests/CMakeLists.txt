enable_testing()

# add all required files for this test
#set(REQUIRED_FILES_FOR_SAMPLE_TEST ../src/SampleClass.cpp)

# create axecutable with source file + additional files
add_executable(SampleTest SampleTest.cpp ${REQUIRED_FILES_FOR_SAMPLE_TEST})
target_include_directories(SampleTest PRIVATE "../src")

#link essential libraries
target_link_libraries(SampleTest PRIVATE
  Qt${QT_VERSION_MAJOR}::Core
  Qt${QT_VERSION_MAJOR}::Quick
  Qt${QT_VERSION_MAJOR}::Test)

# add test
add_test(NAME MySampleTest COMMAND SampleTest)

if(CI_CD MATCHES true)
  windeployqt(SampleTest ${QT_BIN_DIR} ${QML_DIR})
endif()
